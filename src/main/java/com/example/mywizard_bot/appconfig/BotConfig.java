package com.example.mywizard_bot.appconfig;

import com.example.mywizard_bot.MyWizardTelegramBot;
import com.example.mywizard_bot.botapi.TelegramFacade;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Data
@Configuration
@ConfigurationProperties(prefix = "telegrambot")
public class BotConfig  {
    private String webHookPath;
    private String botToken;
    private String botUserName;

    //    private DefaultBotOptions.ProxyType proxyType;
//    private String proxyHost;
//    private int proxyPort;
    @Bean
    public MyWizardTelegramBot MyWizardTelegramBot(TelegramFacade telegramFacade) {
//        DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);
//
//        botOptions.setProxyHost(proxyHost);
//        botOptions.setProxyPort(proxyPort);
//        botOptions.setProxyType(proxyType);

        MyWizardTelegramBot mySuperTelegramBot = new MyWizardTelegramBot(telegramFacade);
        mySuperTelegramBot.setBotToken(botToken);
        mySuperTelegramBot.setBotUsername(botUserName);
        mySuperTelegramBot.setWebHookPath(webHookPath);

        return mySuperTelegramBot;
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();

        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
   }
}
