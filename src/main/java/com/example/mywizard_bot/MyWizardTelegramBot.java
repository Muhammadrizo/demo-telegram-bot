package com.example.mywizard_bot;

import com.example.mywizard_bot.botapi.TelegramFacade;
import com.example.mywizard_bot.cache.UserDataCache;
import com.example.mywizard_bot.modal.UserProfileData;
import com.example.mywizard_bot.qrcode.QRCodeGenerator;
import com.example.mywizard_bot.qrcode.UsersQRCode;
import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import lombok.Data;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.*;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class MyWizardTelegramBot extends TelegramWebhookBot {


    @Value("${filePath}")
    private String filePath;
    private String webHookPath;
    private String botUsername;
    private String botToken;
    private String qrCodeImagePath;
    private static int counter = 0;
    private String filePathToImage = "./src/main/resources/static/images/downloaded/";

    private final TelegramFacade telegramFacade;

    public MyWizardTelegramBot(TelegramFacade telegramFacade) {
        this.telegramFacade = telegramFacade;
    }


    @Override
    public String getBotUsername() {
        return this.botUsername;
    }

    @Override
    public String getBotToken() {
        return this.botToken;
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {

        return telegramFacade.handleUpdate(update);
    }

    @Override
    public String getBotPath() {
        return this.botUsername;
    }

    public void sendPhoto(long chatId, String imageCaption, String imagePath) throws FileNotFoundException, TelegramApiException {
        File image = ResourceUtils.getFile("classpath:" + imagePath);
        SendPhoto sendPhoto = new SendPhoto().setPhoto(image);
        sendPhoto.setChatId(chatId);
        sendPhoto.setCaption(imageCaption);
        execute(sendPhoto);
    }

    @SneakyThrows
    public void sendDocument(long chatId, String caption, File sendFile) {
        SendDocument sendDocument = new SendDocument();
        sendDocument.setChatId(chatId);
        sendDocument.setCaption(caption);
        sendDocument.setDocument(sendFile);
        execute(sendDocument);
    }

    @SneakyThrows
    public void generateQRCode(Message message, UserProfileData user) {
        long chatId = message.getChatId();
        int userId = message.getFrom().getId();
        UsersQRCode usersQRCode = new UsersQRCode(userId, user.getName(), user.getAge());
        String qrCodePath = user.getName() + ".png";
        qrCodeImagePath = filePath + qrCodePath;
        QRCodeGenerator.generateQRCodeImage(usersQRCode.toString(), 250, 250, qrCodeImagePath);
//        sendPhoto(chatId,"Ваши данние на формате QR Code","static/images/" + qrCodePath);
        File image = ResourceUtils.getFile(filePath + qrCodePath);
        SendPhoto sendPhoto = new SendPhoto().setPhoto(image);
        sendPhoto.setChatId(chatId);
        sendPhoto.setCaption("Ваши данние " + user.getName() + " на формате QR Code");
        execute(sendPhoto);
    }

    @SneakyThrows
    public void readQR(Message msg) {
//        QRCodeGenerator.readQRCode(qrCodeImagePath);

//        String fileName = "";
//        String fileId = "";
//        if (msg != null && msg.hasDocument()) {
//             Document doc = msg.getDocument();
//             fileName = doc.getFileName();
//             fileId = doc.getFileId();
//           uploadFile(fileName,fileId);
//        }else
//            System.out.println("OOOOOOOOO shiiit");
    }


    public boolean uploadFile(String fileName, String fileId) {
        try {
            URL url = new URL("https://api.telegram.org/bot" + botToken + "/getFile?file_id=" + fileId);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String getFileResponse = br.readLine();

            JSONObject jresult = new JSONObject(getFileResponse);
            JSONObject path = jresult.getJSONObject("result");
            String filePath = path.getString("file_path");
            System.out.println(filePath);
            File localFile = new File("src/main/resources/uploaded_files/" + fileName);
            InputStream is = new URL("https://api.telegram.org/file/bot" + botToken + "/" + filePath).openStream();

            FileUtils.copyInputStreamToFile(is, localFile);

            br.close();
            is.close();

            System.out.println("Uploaded");
            return true;
        } catch (Exception e) {
            System.out.println("Nimadir xato");
            return false;
        }
    }

    @SneakyThrows
    public void uploadPhoto(Message message, UserProfileData user2) {
        List<PhotoSize> photos = message.getPhoto();
        String pathImage = user2.getName() + ".png";
        String photoPath = filePath + pathImage;

        String f_id = photos.stream()
                .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
                .findFirst()
                .orElse(null).getFileId();

        URL url = new URL("https://api.telegram.org/bot" + botToken + "/getFile?file_id=" + f_id);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String getFileResponse = br.readLine();

        JSONObject jresult = new JSONObject(getFileResponse);
        JSONObject path = jresult.getJSONObject("result");
        String filePath1 = path.getString("file_path");
        System.out.println(filePath1);

        photoPath = "./src/main/resources/static/images/downloaded/"+pathImage;
        File localFile = new File(photoPath);
        InputStream is = new URL("https://api.telegram.org/file/bot" + botToken + "/" + filePath1).openStream();
        FileUtils.copyInputStreamToFile(is, localFile);

        System.out.println("Photo downloaded");
//        JSONObject object = new JSONObject();
//        System.out.println(object.toString());
//        JSONObject user = object.getJSONObject("UsersQRCode");
//        String userName = user.getString("username");
        String result = QRCodeGenerator.readQRCode(photoPath);
        File file =ResourceUtils.getFile("classpath:static/docs/user_profile.txt");
        sendDocument(message.getChatId(), "Ваша анкета вам представлял с помощью QR Code /n По вашему QR Code написинно " + result, file);

    }
}
