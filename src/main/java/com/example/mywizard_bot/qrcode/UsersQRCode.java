package com.example.mywizard_bot.qrcode;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UsersQRCode {
    private int userId;
    private String username;
    private int age;

    @Override
    public String toString() {
        return "UsersQRCode{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", age=" + age +
                '}';
    }
}
