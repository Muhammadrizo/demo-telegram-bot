package com.example.mywizard_bot.cache;

import com.example.mywizard_bot.botapi.BotState;
import com.example.mywizard_bot.modal.UserProfileData;

public interface DataCache {
    BotState getUsersCurrentBotState(int userId);

    void setUsersCurrentBotState(int userId, BotState botState);

    UserProfileData getUserProfileData(int userId);

    void saveUserProfileData(int userId, UserProfileData userProfileData);
}
