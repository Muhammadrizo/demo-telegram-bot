package com.example.mywizard_bot.cache;

import com.example.mywizard_bot.botapi.BotState;
import com.example.mywizard_bot.modal.UserProfileData;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserDataCache implements DataCache {

    private Map<Integer, BotState> userBotStates = new HashMap<>();
    private Map<Integer, UserProfileData> usersProfileData = new HashMap<>();

    @Override
    public BotState getUsersCurrentBotState(int userId) {
        BotState botState = userBotStates.get(userId);

        if (botState == null) {
            botState = BotState.ASK_DESTINY;
        }

        return botState;
    }

    @Override
    public void setUsersCurrentBotState(int userId, BotState botState) {
        userBotStates.put(userId, botState);
    }

    @Override
    public UserProfileData getUserProfileData(int userId) {
        UserProfileData userProfileData = usersProfileData.get(userId);

        if (userProfileData == null){
            userProfileData = new UserProfileData();
        }

        return userProfileData;
    }

    @Override
    public void saveUserProfileData(int userId, UserProfileData userProfileData) {
        usersProfileData.put(userId,userProfileData);
    }
}
