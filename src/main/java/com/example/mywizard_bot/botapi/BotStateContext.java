package com.example.mywizard_bot.botapi;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BotStateContext {
    private Map<BotState, InputMessageHandler> messageHandlers = new HashMap<>();

    public BotStateContext(List<InputMessageHandler> messageHandlers) {
        for (InputMessageHandler handler : messageHandlers) {
            this.messageHandlers.put(handler.getHandlerName(), handler);
        }
    }

    public SendMessage processInputMessage(BotState currentState, Message message) {
        InputMessageHandler currentMessageHandler = findMessageHandler(currentState);
        return currentMessageHandler.handle(message);
    }

    private InputMessageHandler findMessageHandler(BotState currentState) {
        if (isFillingProfileState(currentState)) {
            return messageHandlers.get(BotState.FILLING_PROFILE);
        }

        return messageHandlers.get(currentState);
    }

    private boolean isFillingProfileState(BotState currentState) {
        switch (currentState) {
            case ASK_AGE:
            case ASK_NAME:
            case ASK_SONG:
            case ASK_COLOR:
            case ASK_MOVIE:
            case ASK_GENDER:
            case ASK_NUMBER:
            case FILLING_PROFILE:
            case PROFILE_FILLED:
            case WAITING_FOR_FILE_UPLOAD:
                return true;
            default:
                return false;
        }
    }


}
