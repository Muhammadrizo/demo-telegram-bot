package com.example.mywizard_bot.botapi;

import com.example.mywizard_bot.MyWizardTelegramBot;
import com.example.mywizard_bot.modal.UserProfileData;
import com.example.mywizard_bot.cache.UserDataCache;
import com.example.mywizard_bot.service.MainMenuService;
import com.example.mywizard_bot.service.ReplyMessageService;
import com.example.mywizard_bot.utils.UploadAnyTypeFiles;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Comparator;
import java.util.List;

@Component
public class TelegramFacade {
    private final BotStateContext botStateContext;
    private final UserDataCache userDataCache;
    private final MainMenuService mainMenuService;
    private final ReplyMessageService replyMessageService;
    private final MyWizardTelegramBot myWizardTelegramBot;

    private static Logger logger = LoggerFactory.getLogger(TelegramFacade.class);

    public TelegramFacade(BotStateContext botStateContext,
                          UserDataCache userDataCache,
                          MainMenuService mainMenuService,
                          ReplyMessageService replyMessageService,
                          @Lazy MyWizardTelegramBot myWizardTelegramBot) {
        this.botStateContext = botStateContext;
        this.userDataCache = userDataCache;
        this.mainMenuService = mainMenuService;
        this.replyMessageService = replyMessageService;
        this.myWizardTelegramBot = myWizardTelegramBot;
    }

    public BotApiMethod<?> handleUpdate(Update update) {
        SendMessage replyMessage = null;

        if (update.hasMessage() && update.getMessage().hasPhoto()){
//            myWizardTelegramBot.readQR(update.getMessage());
//            userDataCache.setUsersCurrentBotState(userId, botState);

            replyMessage = botStateContext.processInputMessage(BotState.WAITING_FOR_FILE_UPLOAD, update.getMessage());
            return replyMessage;
        }

        if (update.hasCallbackQuery()) {
            CallbackQuery callbackQuery = update.getCallbackQuery();
            logger.info("New CallBackQuery from User: {}, userId: {}, with data: {}", update.getCallbackQuery().getFrom().getUserName(),
                    callbackQuery.getFrom().getId(), update.getCallbackQuery().getData());
            return processCallBackQuery(callbackQuery);
        }
        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            logger.info("New message from User: {}, userId: {}, chatID: {}, with text: {}", message.getFrom().getUserName(),
                    message.getFrom().getId(), message.getChatId(), message.getText());
            replyMessage = handleInputUpdate(message);
        }

        return replyMessage;
}

    @SneakyThrows
    private SendMessage handleInputUpdate(Message message) {
        String inputMessage = message.getText();
        int userId = message.getFrom().getId();
        long chatId = message.getChatId();
        BotState botState;
        SendMessage replyMessage;
        switch (inputMessage) {
            case "/start":
                botState = BotState.ASK_DESTINY;
                myWizardTelegramBot.sendPhoto(chatId, replyMessageService.getReplyText("reply.hello"), "static/images/images.jpg");
                break;
            case "Получить предсказание":
                botState = BotState.FILLING_PROFILE;
                break;
            case "Моя анкета":
                botState = BotState.SHOW_USER_PROFILE;
                break;
            case "Генераться QR код":
                myWizardTelegramBot.generateQRCode(message, userDataCache.getUserProfileData(userId));
                botState = BotState.SHOW_MAIN_MENU;
                break;
            case "Скачать анкету с QR кодом":
//                myWizardTelegramBot.readQR(userDataCache.getUserProfileData(userId), message.getDocument());
                botState = BotState.WAITING_FOR_FILE_UPLOAD;
                break;
            case "Скачать анкету":
                myWizardTelegramBot.sendDocument(chatId, "Ваша анкета", getUsersProfile(userId,message));
                botState = BotState.SHOW_USER_PROFILE;
                break;
            case "Помощь":
                botState = BotState.SHOW_HELP_MENU;
                break;
            default:
                botState = userDataCache.getUsersCurrentBotState(userId);
                break;
        }

        userDataCache.setUsersCurrentBotState(userId, botState);

        replyMessage = botStateContext.processInputMessage(botState, message);

        return replyMessage;
    }

    @SneakyThrows
    private File getUsersProfile(int userId, Message message) {
        UserProfileData userProfileData = userDataCache.getUserProfileData(userId);
//        File newFile = new File("classpath:static/docs/" + userProfileData.getName() + ".txt");
        File profileFile = ResourceUtils.getFile("classpath:static/docs/user_profile.txt");

        try (FileWriter fw = new FileWriter(profileFile.getAbsoluteFile());
             BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(userProfileData.toString());
        }
        return profileFile;
    }


    private BotApiMethod<?> processCallBackQuery(CallbackQuery buttonQuery) {
        final long chatId = buttonQuery.getMessage().getChatId();
        final int userId = buttonQuery.getFrom().getId();

        BotApiMethod<?> callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Воспользуйтесь главным меню");

        String buttonQueryData = buttonQuery.getData();
        // From Destiny choose button
        if (buttonQueryData.equals("buttonYes")) {
//            callBackAnswer = new SendMessage(chatId, "reply.askName");
            callBackAnswer = replyMessageService.getReplayMessage(chatId, "reply.askName");
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_AGE);
        } else if (buttonQueryData.equals("buttonNo")) {
            callBackAnswer = sendAnswerCallbackQuery("Возвращайся, когда будеш готов", false, buttonQuery);
        } else if (buttonQueryData.equals("buttonIwillThink")) {
            callBackAnswer = sendAnswerCallbackQuery("Данная кнопка не поддерживается", true, buttonQuery);
        }
        // From Gender chose button
        else if (buttonQueryData.equals("buttonMan")) {
            UserProfileData userProfileData = userDataCache.getUserProfileData(userId);
            userProfileData.setGender("М");
            userDataCache.saveUserProfileData(userId, userProfileData);
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_COLOR);
            callBackAnswer = replyMessageService.getReplayMessage(chatId, "reply.askNumber");
        } else if (buttonQueryData.equals("buttonWoman")) {
            UserProfileData userProfileData = userDataCache.getUserProfileData(userId);
            userProfileData.setGender("Ж");
            userDataCache.saveUserProfileData(userId, userProfileData);
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_COLOR);
//            callBackAnswer = new SendMessage(chatId, "reply.askNumber");
            callBackAnswer = replyMessageService.getReplayMessage(chatId, "reply.askNumber");
        } else {
            userDataCache.setUsersCurrentBotState(userId, BotState.SHOW_MAIN_MENU);
        }

        return callBackAnswer;
    }

    private AnswerCallbackQuery sendAnswerCallbackQuery(String text, boolean alert, CallbackQuery buttonQuery) {
        AnswerCallbackQuery answerCallbackQuery = new AnswerCallbackQuery();
        answerCallbackQuery.setCallbackQueryId(buttonQuery.getId());
        answerCallbackQuery.setShowAlert(alert);
        answerCallbackQuery.setText(text);
        return answerCallbackQuery;
    }
}
