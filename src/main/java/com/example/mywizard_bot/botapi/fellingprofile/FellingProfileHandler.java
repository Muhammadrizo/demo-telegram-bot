package com.example.mywizard_bot.botapi.fellingprofile;

import com.example.mywizard_bot.MyWizardTelegramBot;
import com.example.mywizard_bot.botapi.BotState;
import com.example.mywizard_bot.botapi.InputMessageHandler;
import com.example.mywizard_bot.cache.UserDataCache;
import com.example.mywizard_bot.modal.UserProfileData;
import com.example.mywizard_bot.service.PredictionService;
import com.example.mywizard_bot.service.ReplyMessageService;
import com.example.mywizard_bot.utils.Emojis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@Component
public class FellingProfileHandler implements InputMessageHandler {
    private UserDataCache userDataCache;
    private ReplyMessageService messageService;
    private final PredictionService predictionService;
    private final MyWizardTelegramBot myWizardTelegramBot;
    private static Logger logger = LoggerFactory.getLogger(FellingProfileHandler.class);

    public FellingProfileHandler(UserDataCache userDataCache,
                                 ReplyMessageService messageService,
                                 PredictionService predictionService,
                                 @Lazy MyWizardTelegramBot myWizardTelegramBot) {
        this.userDataCache = userDataCache;
        this.messageService = messageService;
        this.predictionService = predictionService;
        this.myWizardTelegramBot = myWizardTelegramBot;
    }

    @Override
    public SendMessage handle(Message message) {
        if (userDataCache.getUsersCurrentBotState(message.getFrom().getId()).equals(BotState.FILLING_PROFILE)) {
            userDataCache.setUsersCurrentBotState(message.getFrom().getId(), BotState.ASK_NAME);
        }

        return processUsersInput(message);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.FILLING_PROFILE;
    }

    private SendMessage processUsersInput(Message inputMsg) {
        String usersAnswer = inputMsg.getText();
        int userId = inputMsg.getFrom().getId();
        long chatId = inputMsg.getChatId();

        UserProfileData profileData = userDataCache.getUserProfileData(userId);
        BotState botState = userDataCache.getUsersCurrentBotState(userId);

        SendMessage replyToUser = null;

        if (botState.equals(BotState.ASK_NAME)) {
            replyToUser = messageService.getReplayMessage(chatId, "reply.askName");
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_AGE);
        }

        if (botState.equals(BotState.ASK_AGE)) {
            profileData.setName(usersAnswer);
            replyToUser = messageService.getReplayMessage(chatId, "reply.askAge");
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_GENDER);
        }

        if (botState.equals(BotState.ASK_GENDER)) {
            profileData.setAge(Integer.parseInt(usersAnswer));
            replyToUser = messageService.getReplayMessage(chatId, "reply.askGender");
//            userDataCache.setUsersCurrentBotState(userId,BotState.ASK_NUMBER);
            replyToUser.setReplyMarkup(getGenderButtonsMarkup());
        }

        if (botState.equals(BotState.ASK_NUMBER)) {
            profileData.setGender(usersAnswer);
            replyToUser = messageService.getReplayMessage(chatId, "reply.askNumber");
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_COLOR);
        }

        if (botState.equals(BotState.ASK_COLOR)) {
            profileData.setNumber(Integer.parseInt(usersAnswer));
            replyToUser = messageService.getReplayMessage(chatId, "reply.askColor");
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_MOVIE);
        }

        if (botState.equals(BotState.ASK_MOVIE)) {
            profileData.setColor(usersAnswer);
            replyToUser = messageService.getReplayMessage(chatId, "reply.askMovie");
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_SONG);
        }

        if (botState.equals(BotState.ASK_SONG)) {
            profileData.setMovie(usersAnswer);
            replyToUser = messageService.getReplayMessage(chatId, "reply.askSong");
            userDataCache.setUsersCurrentBotState(userId, BotState.PROFILE_FILLED);
        }
        if (botState.equals(BotState.WAITING_FOR_FILE_UPLOAD)) {
            if (inputMsg.getPhoto() != null){
                myWizardTelegramBot.uploadPhoto(inputMsg,userDataCache.getUserProfileData(userId));
                userDataCache.setUsersCurrentBotState(userId, BotState.SHOW_MAIN_MENU);
            }else{
                replyToUser = messageService.getReplayMessage(chatId, "reply.waitingForUploadFile");
                userDataCache.setUsersCurrentBotState(userId, BotState.WAITING_FOR_FILE_UPLOAD);
            }
//            myWizardTelegramBot.readQR(inputMsg);
        }
        if (botState.equals(BotState.PROFILE_FILLED)) {
            profileData.setSong(usersAnswer);
            userDataCache.setUsersCurrentBotState(userId, BotState.SHOW_MAIN_MENU);

            String profileFilledMessage = messageService.getReplyText("reply.profileFilled", profileData.getName(), Emojis.SPARKLES);
            String prodictionMessage = predictionService.getPridiction();

            replyToUser = new SendMessage(chatId, String.format("%s%n%n%s %s ", profileFilledMessage, Emojis.SCROLL, prodictionMessage));
            replyToUser.setParseMode("HTML");
            logger.info("User => " + profileData.toString() + " full info");
        }

        userDataCache.saveUserProfileData(userId, profileData);
        return replyToUser;
    }

    private InlineKeyboardMarkup getGenderButtonsMarkup() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton buttonGenderMan = new InlineKeyboardButton().setText("М");
        InlineKeyboardButton buttonGenderWoman = new InlineKeyboardButton().setText("Ж");

        buttonGenderMan.setCallbackData("buttonMan");
        buttonGenderWoman.setCallbackData("buttonWoman");

        List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();

        keyboardButtonsRow1.add(buttonGenderMan);
        keyboardButtonsRow1.add(buttonGenderWoman);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonsRow1);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;
    }
}
