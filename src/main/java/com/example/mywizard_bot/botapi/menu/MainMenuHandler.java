package com.example.mywizard_bot.botapi.menu;

import com.example.mywizard_bot.botapi.BotState;
import com.example.mywizard_bot.botapi.InputMessageHandler;
import com.example.mywizard_bot.service.MainMenuService;
import com.example.mywizard_bot.service.ReplyMessageService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
public class MainMenuHandler implements InputMessageHandler {
    private final ReplyMessageService messageService;
    private final MainMenuService menuService;
    public MainMenuHandler(ReplyMessageService messageService, MainMenuService menuService) {
        this.messageService = messageService;
        this.menuService = menuService;
    }

    @Override
    public SendMessage handle(Message message) {
        return menuService.getMainMenuMessage(message.getChatId(),messageService.getReplyText("reply.showMainMenu"));
    }

    @Override
    public BotState getHandlerName() {
        return BotState.SHOW_MAIN_MENU;
    }
}
