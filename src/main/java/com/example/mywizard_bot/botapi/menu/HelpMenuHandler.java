package com.example.mywizard_bot.botapi.menu;

import com.example.mywizard_bot.botapi.BotState;
import com.example.mywizard_bot.botapi.InputMessageHandler;
import com.example.mywizard_bot.service.MainMenuService;
import com.example.mywizard_bot.service.ReplyMessageService;
import com.example.mywizard_bot.utils.Emojis;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
public class HelpMenuHandler implements InputMessageHandler {
    private final MainMenuService mainMenuService;
    private final ReplyMessageService replyMessageService;

    public HelpMenuHandler(MainMenuService mainMenuHandler, ReplyMessageService replyMessageService) {
        this.mainMenuService = mainMenuHandler;
        this.replyMessageService = replyMessageService;
    }

    @Override
    public SendMessage handle(Message message) {
        return mainMenuService.getMainMenuMessage(message.getChatId(),
                replyMessageService.getReplyText("reply.showHelpMenu",Emojis.MAGE));
    }

    @Override
    public BotState getHandlerName() {
        return BotState.SHOW_HELP_MENU;
    }
}
