package com.example.mywizard_bot.botapi.askdestiny;

import com.example.mywizard_bot.botapi.BotState;
import com.example.mywizard_bot.botapi.InputMessageHandler;
import com.example.mywizard_bot.service.ReplyMessageService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@Component
public class AskDestinyHandler implements InputMessageHandler {
//    private final UserDataCache userDataCache;
    private final ReplyMessageService messageService;

    public AskDestinyHandler(/*UserDataCache userDataCache,*/ ReplyMessageService messageService) {
//        this.userDataCache = userDataCache;
        this.messageService = messageService;
    }


    @Override
    public SendMessage handle(Message message) {

        return processUsersInput(message);

    }

    @Override
    public BotState getHandlerName() {
        return BotState.ASK_DESTINY;
    }

    private SendMessage processUsersInput(Message inputMessage) {
//        int userID = inputMessage.getFrom().getId();
        long chatId = inputMessage.getChatId();

        SendMessage replyToUser = messageService.getReplayMessage(chatId, "reply.askDestiny");
        replyToUser.setReplyMarkup(getInlineMessageButton());
//        userDataCache.setUsersCurrentBotState(userID,BotState.FILLING_PROFILE);

        return replyToUser;
    }

    private InlineKeyboardMarkup getInlineMessageButton(){
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton buttonYes = new InlineKeyboardButton().setText("Да");
        InlineKeyboardButton buttonNo = new InlineKeyboardButton().setText("Нет спосибо");
        InlineKeyboardButton buttonIwillThink = new InlineKeyboardButton().setText("Я подумаю");
        InlineKeyboardButton buttonIDontKnow = new InlineKeyboardButton().setText("Еще не определился");

        buttonYes.setCallbackData("buttonYes");
        buttonNo.setCallbackData("buttonNo");
        buttonIwillThink.setCallbackData("buttonIwillThink");
        buttonIDontKnow.setCallbackData("-");

        List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
        keyboardButtonsRow1.add(buttonYes);
        keyboardButtonsRow1.add(buttonNo);

        List<InlineKeyboardButton> keyboardButtonsRow2 = new ArrayList<>();
        keyboardButtonsRow2.add(buttonIwillThink);
        keyboardButtonsRow2.add(buttonIDontKnow);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonsRow1);
        rowList.add(keyboardButtonsRow2);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;
     }
}
