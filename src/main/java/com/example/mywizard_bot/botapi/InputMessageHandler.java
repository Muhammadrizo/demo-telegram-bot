package com.example.mywizard_bot.botapi;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

/**Abrabotchik soobshenie*/
public interface InputMessageHandler {
        SendMessage handle(Message message);

        BotState getHandlerName();
}
