package com.example.mywizard_bot.botapi;

import com.example.mywizard_bot.service.ReplyMessageService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

@Component
public class UpdateFileHandler implements InputMessageHandler {
    private final ReplyMessageService replyMessageService;

    public UpdateFileHandler(ReplyMessageService replyMessageService) {
        this.replyMessageService = replyMessageService;
    }

    @Override
    public SendMessage handle(Message message) {
        return new SendMessage(message.getChatId(),replyMessageService.getReplyText("reply.waitingForUploadFile"));
    }

    @Override
    public BotState getHandlerName() {
        return BotState.WAITING_FOR_FILE_UPLOAD;
    }
}
