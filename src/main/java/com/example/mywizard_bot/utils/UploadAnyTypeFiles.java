package com.example.mywizard_bot.utils;

import com.example.mywizard_bot.MyWizardTelegramBot;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Comparator;
import java.util.List;

@Component
public class UploadAnyTypeFiles {
    @Value("${telegrambot.botToken}")
    private static String getBotToken;
//    private final static String urlToTelegram = ""
    private final MyWizardTelegramBot myWizardTelegramBot;

    public UploadAnyTypeFiles(MyWizardTelegramBot myWizardTelegramBot) {
        this.myWizardTelegramBot = myWizardTelegramBot;
    }

    public static void uploadPhoto(Update update) {

        System.out.println("Chat Id --- > " + update.getMessage().getChatId());
        if (update.hasMessage() && update.getMessage().hasPhoto()) {
            long chat_id = update.getMessage().getChatId();
            List<PhotoSize> photos = update.getMessage().getPhoto();
            PhotoSize photoSize = photos.get(0);
            // Know file_id
            String f_id = photos.stream()
                    .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
                    .findFirst()
                    .orElse(null).getFileId();
            // Know photo width
            File localFile = new File("src/main/resources/uploaded_files/"+"image1.png");
//            FileUtils.copyInputStreamToFile(f_id,localFile);
            int f_width = photos.stream()
                    .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
                    .findFirst()
                    .orElse(null).getWidth();
            // Know photo height
            int f_height = photos.stream()
                    .sorted(Comparator.comparing(PhotoSize::getFileSize).reversed())
                    .findFirst()
                    .orElse(null).getHeight();
            uploadPhotoFromTelegram(f_id);
            // Set photo caption
//            String caption = "file_id: " + f_id + "\nwidth: " + Integer.toString(f_width) + "\nheight: " + Integer.toString(f_height);
//            SendPhoto msg = new SendPhoto()
//                    .setChatId(chat_id)
//                    .setPhoto(f_id)
//                    .setCaption(caption);
        }
    }

    @SneakyThrows
    private static void uploadPhotoFromTelegram(String fileId){
        URL url = new URL("https://api.telegram.org/bot" + "1675091781:AAGzU2R3gGHYnBAk91Tlnac7J7GR9FzSK_c" + "/getFile?file_id="+ fileId);
        System.out.println("FileId ----->" + fileId + "chatId + ");
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String getFileResponse = br.readLine();

        JSONObject jresult = new JSONObject(getFileResponse);
        JSONObject path = jresult.getJSONObject("result");
        String filePath =  path.getString("file_path");
        System.out.println(filePath);

        File localFile = new File("src/main/resources/uploaded_files/"+"image1.png");
        InputStream is = new URL("https://api.telegram.org/bot" + "1675091781:AAGzU2R3gGHYnBAk91Tlnac7J7GR9FzSK_c" + "/" + filePath).openStream();

        FileUtils.copyInputStreamToFile(is,localFile);

        br.close();
        is.close();

        System.out.println("Uploaded");
    }
}
