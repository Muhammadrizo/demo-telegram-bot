package com.example.mywizard_bot.service;

import org.springframework.stereotype.Service;

@Service
public class PredictionService {
    private final ReplyMessageService messageService;

    public PredictionService(ReplyMessageService messageService) {
        this.messageService = messageService;
    }

    public String getPridiction(){
        return messageService.getReplyText("reply.prediction");
    }
}
