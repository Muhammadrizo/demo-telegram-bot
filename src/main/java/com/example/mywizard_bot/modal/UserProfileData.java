package com.example.mywizard_bot.modal;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserProfileData implements Serializable {
    String name;
    int age;
    String gender;
    int number;
    String color;
    String movie;
    String song;

    @Override
    public String toString() {
        return String.format("%s%n------------------------%nИмя: %s%n" +
                        "Возрост: %d%nПол: %s%nЛюбимая номер: %d%nЦвет: %s%nФильм: %s%nПесня: %s%n",
                "Данные по вашей анкете", getName(), getAge(), getGender(), getNumber(),
                getColor(), getMovie(), getSong());
    }
}
