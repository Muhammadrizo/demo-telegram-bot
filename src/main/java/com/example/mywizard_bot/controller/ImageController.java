package com.example.mywizard_bot.controller;

import com.example.mywizard_bot.qrcode.QRCodeGenerator;
import com.google.zxing.NotFoundException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/path")
public class ImageController {
    private String photoUrl = "G:\\Projects\\TelegramRZD_bot\\downloadPhoto\\src\\main\\resources\\static";

    @GetMapping("/get/{url}")
    public String qrImage(@PathVariable String url){
        String info = "";
        try {
            info = QRCodeGenerator.readQRCode(photoUrl + "\\" + url);

        } catch (IOException e) {
            info = "Xatolik";
            e.printStackTrace();
        } catch (NotFoundException e) {
            info = "Xatolik";
            e.printStackTrace();
        }
        return info;

    }
}
